package com.flycms.modules.fullname.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.fullname.domain.Surname;
import org.springframework.stereotype.Repository;

/**
 * 姓氏Mapper接口
 * 
 * @author admin
 * @date 2020-10-13
 */
@Repository
public interface SurnameMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增姓氏
     *
     * @param surname 姓氏
     * @return 结果
     */
    public int insertSurname(Surname surname);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除姓氏
     *
     * @param id 姓氏ID
     * @return 结果
     */
    public int deleteSurnameById(Long id);

    /**
     * 批量删除姓氏
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSurnameByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改姓氏
     *
     * @param surname 姓氏
     * @return 结果
     */
    public int updateSurname(Surname surname);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验姓氏是否唯一
     *
     * @param surname 姓氏ID
     * @return 结果
     */
    public int checkSurnameLastNameUnique(Surname  surname);


    /**
     * 查询姓氏
     * 
     * @param id 姓氏ID
     * @return 姓氏
     */
    public Surname findSurnameById(Long id);

    /**
     * 查询姓氏数量
     *
     * @param pager 分页处理类
     * @return 姓氏数量
     */
    public int querySurnameTotal(Pager pager);

    /**
     * 查询姓氏列表
     * 
     * @param pager 分页处理类
     * @return 姓氏集合
     */
    public List<Surname> selectSurnamePager(Pager pager);

}
