package com.flycms.modules.notify.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.notify.domain.Email;
import com.flycms.modules.notify.domain.dto.EmailDTO;
import com.flycms.modules.notify.service.IEmailService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 邮件服务器Controller
 * 
 * @author admin
 * @date 2020-11-09
 */
@RestController
@RequestMapping("/system/notify/email")
public class EmailAdminController extends BaseController
{
    @Autowired
    private IEmailService emailService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增邮件服务器
     */
    @PreAuthorize("@ss.hasPermi('notify:email:add')")
    @Log(title = "邮件服务器", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Email email)
    {
        if (UserConstants.NOT_UNIQUE.equals(emailService.checkEmailMailServerUnique(email.getMailServer())))
        {
            return AjaxResult.error("新增邮件服务器'" + email.getMailServer() + "'失败，发件箱KEY已存在");
        }
        return toAjax(emailService.insertEmail(email));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除邮件服务器
     */
    @PreAuthorize("@ss.hasPermi('notify:email:remove')")
    @Log(title = "邮件服务器", businessType = BusinessType.DELETE)
    @DeleteMapping("/{mailServers}")
    public AjaxResult remove(@PathVariable String[] mailServers)
    {
        return toAjax(emailService.deleteEmailByIds(mailServers));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改邮件服务器
     */
    @PreAuthorize("@ss.hasPermi('notify:email:edit')")
    @Log(title = "邮件服务器", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Email email)
    {
        if (UserConstants.NOT_UNIQUE.equals(emailService.checkEmailMailServerUnique(email.getMailServer())))
        {
            return AjaxResult.error("新增邮件服务器'" + email.getMailServer() + "'失败，发件箱KEY已存在");
        }
        return toAjax(emailService.updateEmail(email));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询邮件服务器列表
     */
    @PreAuthorize("@ss.hasPermi('notify:email:list')")
    @GetMapping("/list")
    public TableDataInfo list(Email email,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<EmailDTO> pager = emailService.selectEmailPager(email, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出邮件服务器列表
     */
    @PreAuthorize("@ss.hasPermi('notify:email:export')")
    @Log(title = "邮件服务器", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Email email)
    {
        List<EmailDTO> emailList = emailService.exportEmailList(email);
        ExcelUtil<EmailDTO> util = new ExcelUtil<EmailDTO>(EmailDTO.class);
        return util.exportExcel(emailList, "email");
    }

    /**
     * 获取邮件服务器详细信息
     */
    @PreAuthorize("@ss.hasPermi('notify:email:query')")
    @GetMapping(value = "/{mailServer}")
    public AjaxResult getInfo(@PathVariable("mailServer") String mailServer)
    {
        return AjaxResult.success(emailService.findEmailById(mailServer));
    }

}
