package com.flycms.modules.notify.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 邮件服务器数据传输对象 fly_email
 * 
 * @author admin
 * @date 2020-11-09
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class EmailDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 发件箱KEY */
    @Excel(name = "发件箱KEY")
    private String mailServer;
    /** 发件服务器 */
    @Excel(name = "发件服务器")
    private String mailSmtpServer;
    /** 发件人昵称 */
    @Excel(name = "发件人昵称")
    private String mailUserName;
    /** 发件箱账号 */
    @Excel(name = "发件箱账号")
    private String mailSmtpUsermail;
    /** 发件箱密码 */
    @Excel(name = "发件箱密码")
    private String mailSmtpPassword;
    /** 发件箱端口 */
    @Excel(name = "发件箱端口")
    private String mailSmtpPort;

}
