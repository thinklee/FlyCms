package com.flycms.modules.site.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.site.mapper.SiteAboutMapper;
import com.flycms.modules.site.domain.SiteAbout;
import com.flycms.modules.site.domain.dto.SiteAboutDTO;
import com.flycms.modules.site.service.ISiteAboutService;

import java.util.ArrayList;
import java.util.List;

/**
 * 网站相关介绍Service业务层处理
 * 
 * @author admin
 * @date 2021-01-27
 */
@Service
public class SiteAboutServiceImpl implements ISiteAboutService 
{
    @Autowired
    private SiteAboutMapper siteAboutMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增网站相关介绍
     *
     * @param siteAbout 网站相关介绍
     * @return 结果
     */
    @Override
    public int insertSiteAbout(SiteAbout siteAbout)
    {
        siteAbout.setId(SnowFlakeUtils.nextId());
        siteAbout.setCreateTime(DateUtils.getNowDate());
        return siteAboutMapper.insertSiteAbout(siteAbout);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除网站相关介绍
     *
     * @param ids 需要删除的网站相关介绍ID
     * @return 结果
     */
    @Override
    public int deleteSiteAboutByIds(Long[] ids)
    {
        return siteAboutMapper.deleteSiteAboutByIds(ids);
    }

    /**
     * 删除网站相关介绍信息
     *
     * @param id 网站相关介绍ID
     * @return 结果
     */
    @Override
    public int deleteSiteAboutById(Long id)
    {
        return siteAboutMapper.deleteSiteAboutById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改网站相关介绍
     *
     * @param siteAbout 网站相关介绍
     * @return 结果
     */
    @Override
    public int updateSiteAbout(SiteAbout siteAbout)
    {
        return siteAboutMapper.updateSiteAbout(siteAbout);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询网站相关介绍
     * 
     * @param id 网站相关介绍ID
     * @return 网站相关介绍
     */
    @Override
    public SiteAboutDTO findSiteAboutById(Long id)
    {
        SiteAbout siteAbout=siteAboutMapper.findSiteAboutById(id);
        return BeanConvertor.convertBean(siteAbout,SiteAboutDTO.class);
    }


    /**
     * 查询网站相关介绍列表
     *
     * @param siteAbout 网站相关介绍
     * @return 网站相关介绍
     */
    @Override
    public Pager<SiteAboutDTO> selectSiteAboutPager(SiteAbout siteAbout, Integer page, Integer limit, String sort, String order)
    {
        Pager<SiteAboutDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(siteAbout);

        List<SiteAbout> siteAboutList=siteAboutMapper.selectSiteAboutPager(pager);
        List<SiteAboutDTO> dtolsit = new ArrayList<SiteAboutDTO>();
        siteAboutList.forEach(entity -> {
            SiteAboutDTO dto = new SiteAboutDTO();
            dto.setId(entity.getId());
            dto.setClassifyId(entity.getClassifyId());
            dto.setTitle(entity.getTitle());
            dto.setContent(entity.getContent());
            dto.setStatus(entity.getStatus());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(siteAboutMapper.querySiteAboutTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的网站相关介绍列表
     *
     * @param siteAbout 网站相关介绍
     * @return 网站相关介绍集合
     */
    @Override
    public List<SiteAboutDTO> exportSiteAboutList(SiteAbout siteAbout) {
        return BeanConvertor.copyList(siteAboutMapper.exportSiteAboutList(siteAbout),SiteAboutDTO.class);
    }
}
