package com.flycms.modules.site.service.impl;

import com.flycms.common.utils.DateUtils;
import com.flycms.modules.site.domain.Site;
import com.flycms.modules.site.mapper.SiteMapper;
import com.flycms.modules.site.service.ISiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * 官网设置Service业务层处理
 * 
 * @author admin
 * @date 2020-07-08
 */
@Service
public class SiteServiceImpl implements ISiteService
{
    @Autowired
    private SiteMapper siteMapper;


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改官网设置
     *
     * @param site 官网设置
     * @return 结果
     */
    @Override
    public int updateSite(Site site)
    {
        site.setUpdateTime(DateUtils.getNowDate());
        return siteMapper.updateSite(site);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询官网设置
     * 
     * @return 官网设置
     */
    @Override
    //@Cacheable
    public Site selectSite()
    {
        return siteMapper.selectSite();
    }


}
