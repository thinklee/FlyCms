package com.flycms.modules.site.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.site.domain.SiteAbout;
import com.flycms.modules.site.domain.dto.SiteAboutDTO;

import java.util.List;

/**
 * 网站相关介绍Service接口
 * 
 * @author admin
 * @date 2021-01-27
 */
public interface ISiteAboutService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增网站相关介绍
     *
     * @param siteAbout 网站相关介绍
     * @return 结果
     */
    public int insertSiteAbout(SiteAbout siteAbout);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除网站相关介绍
     *
     * @param ids 需要删除的网站相关介绍ID
     * @return 结果
     */
    public int deleteSiteAboutByIds(Long[] ids);

    /**
     * 删除网站相关介绍信息
     *
     * @param id 网站相关介绍ID
     * @return 结果
     */
    public int deleteSiteAboutById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改网站相关介绍
     *
     * @param siteAbout 网站相关介绍
     * @return 结果
     */
    public int updateSiteAbout(SiteAbout siteAbout);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询网站相关介绍
     * 
     * @param id 网站相关介绍ID
     * @return 网站相关介绍
     */
    public SiteAboutDTO findSiteAboutById(Long id);

    /**
     * 查询网站相关介绍列表
     * 
     * @param siteAbout 网站相关介绍
     * @return 网站相关介绍集合
     */
    public Pager<SiteAboutDTO> selectSiteAboutPager(SiteAbout siteAbout, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的网站相关介绍列表
     *
     * @param siteAbout 网站相关介绍
     * @return 网站相关介绍集合
     */
    public List<SiteAboutDTO> exportSiteAboutList(SiteAbout siteAbout);
}
