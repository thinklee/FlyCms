package com.flycms.modules.site.mapper;

import com.flycms.modules.site.domain.Site;
import org.springframework.stereotype.Repository;

/**
 * 官网设置Mapper接口
 * 
 * @author admin
 * @date 2020-07-08
 */
@Repository
public interface SiteMapper
{


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改官网设置
     *
     * @param site 官网设置
     * @return 结果
     */
    public int updateSite(Site site);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////


    /**
     * 查询官网设置
     * 
     * @return 官网设置
     */
    public Site selectSite();



}
