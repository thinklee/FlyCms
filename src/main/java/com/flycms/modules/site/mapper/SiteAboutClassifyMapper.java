package com.flycms.modules.site.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.site.domain.SiteAboutClassify;
import org.springframework.stereotype.Repository;

/**
 * 关于我们分类Mapper接口
 * 
 * @author admin
 * @date 2021-01-27
 */
@Repository
public interface SiteAboutClassifyMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增关于我们分类
     *
     * @param siteAboutClassify 关于我们分类
     * @return 结果
     */
    public int insertSiteAboutClassify(SiteAboutClassify siteAboutClassify);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除关于我们分类
     *
     * @param id 关于我们分类ID
     * @return 结果
     */
    public int deleteSiteAboutClassifyById(Long id);

    /**
     * 批量删除关于我们分类
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSiteAboutClassifyByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改关于我们分类
     *
     * @param siteAboutClassify 关于我们分类
     * @return 结果
     */
    public int updateSiteAboutClassify(SiteAboutClassify siteAboutClassify);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询关于我们分类
     * 
     * @param id 关于我们分类ID
     * @return 关于我们分类
     */
    public SiteAboutClassify findSiteAboutClassifyById(Long id);

    /**
     * 查询关于我们分类数量
     *
     * @param pager 分页处理类
     * @return 关于我们分类数量
     */
    public int querySiteAboutClassifyTotal(Pager pager);

    /**
     * 查询关于我们分类列表
     * 
     * @param pager 分页处理类
     * @return 关于我们分类集合
     */
    public List<SiteAboutClassify> selectSiteAboutClassifyPager(Pager pager);

    /**
     * 查询需要导出的关于我们分类列表
     *
     * @param siteAboutClassify 关于我们分类
     * @return 关于我们分类集合
     */
    public List<SiteAboutClassify> exportSiteAboutClassifyList(SiteAboutClassify siteAboutClassify);
}
