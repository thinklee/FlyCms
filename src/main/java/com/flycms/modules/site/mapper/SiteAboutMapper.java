package com.flycms.modules.site.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.site.domain.SiteAbout;
import org.springframework.stereotype.Repository;

/**
 * 网站相关介绍Mapper接口
 * 
 * @author admin
 * @date 2021-01-27
 */
@Repository
public interface SiteAboutMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增网站相关介绍
     *
     * @param siteAbout 网站相关介绍
     * @return 结果
     */
    public int insertSiteAbout(SiteAbout siteAbout);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除网站相关介绍
     *
     * @param id 网站相关介绍ID
     * @return 结果
     */
    public int deleteSiteAboutById(Long id);

    /**
     * 批量删除网站相关介绍
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSiteAboutByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改网站相关介绍
     *
     * @param siteAbout 网站相关介绍
     * @return 结果
     */
    public int updateSiteAbout(SiteAbout siteAbout);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询网站相关介绍
     * 
     * @param id 网站相关介绍ID
     * @return 网站相关介绍
     */
    public SiteAbout findSiteAboutById(Long id);

    /**
     * 查询网站相关介绍数量
     *
     * @param pager 分页处理类
     * @return 网站相关介绍数量
     */
    public int querySiteAboutTotal(Pager pager);

    /**
     * 查询网站相关介绍列表
     * 
     * @param pager 分页处理类
     * @return 网站相关介绍集合
     */
    public List<SiteAbout> selectSiteAboutPager(Pager pager);

    /**
     * 查询需要导出的网站相关介绍列表
     *
     * @param siteAbout 网站相关介绍
     * @return 网站相关介绍集合
     */
    public List<SiteAbout> exportSiteAboutList(SiteAbout siteAbout);
}
