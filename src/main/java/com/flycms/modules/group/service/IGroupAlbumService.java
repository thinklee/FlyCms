package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupAlbum;
import com.flycms.modules.group.domain.dto.GroupAlbumDTO;

import java.util.List;

/**
 * 小组帖子专辑Service接口
 * 
 * @author admin
 * @date 2020-12-16
 */
public interface IGroupAlbumService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组帖子专辑
     *
     * @param groupAlbum 小组帖子专辑
     * @return 结果
     */
    public int insertGroupAlbum(GroupAlbum groupAlbum);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组帖子专辑
     *
     * @param ids 需要删除的小组帖子专辑ID
     * @return 结果
     */
    public int deleteGroupAlbumByIds(Long[] ids);

    /**
     * 删除小组帖子专辑信息
     *
     * @param id 小组帖子专辑ID
     * @return 结果
     */
    public int deleteGroupAlbumById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组帖子专辑
     *
     * @param groupAlbum 小组帖子专辑
     * @return 结果
     */
    public int updateGroupAlbum(GroupAlbum groupAlbum);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验专辑名字是否唯一
     *
     * @param id 专辑ID
     * @param album_name 专辑名字
     * @return 结果
     */
    public String checkGroupAlbumAlbumNameUnique(Long id,String album_name);


    /**
     * 查询小组帖子专辑
     * 
     * @param id 小组帖子专辑ID
     * @return 小组帖子专辑
     */
    public GroupAlbumDTO findGroupAlbumById(Long id);

    /**
     * 查询小组帖子专辑列表
     * 
     * @param groupAlbum 小组帖子专辑
     * @return 小组帖子专辑集合
     */
    public Pager<GroupAlbumDTO> selectGroupAlbumPager(GroupAlbum groupAlbum, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的小组帖子专辑列表
     *
     * @param groupAlbum 小组帖子专辑
     * @return 小组帖子专辑集合
     */
    public List<GroupAlbumDTO> exportGroupAlbumList(GroupAlbum groupAlbum);
}
