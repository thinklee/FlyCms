package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupAlbumTopic;
import com.flycms.modules.group.domain.dto.GroupAlbumTopicDTO;

import java.util.List;

/**
 * 小组专辑帖子关联Service接口
 * 
 * @author admin
 * @date 2020-12-16
 */
public interface IGroupAlbumTopicService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组专辑帖子关联
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 结果
     */
    public int insertGroupAlbumTopic(GroupAlbumTopic groupAlbumTopic);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组专辑帖子关联
     *
     * @param ids 需要删除的小组专辑帖子关联ID
     * @return 结果
     */
    public int deleteGroupAlbumTopicByIds(Long[] ids);

    /**
     * 删除小组专辑帖子关联信息
     *
     * @param id 小组专辑帖子关联ID
     * @return 结果
     */
    public int deleteGroupAlbumTopicById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组专辑帖子关联
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 结果
     */
    public int updateGroupAlbumTopic(GroupAlbumTopic groupAlbumTopic);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询小组专辑帖子关联
     * 
     * @param id 小组专辑帖子关联ID
     * @return 小组专辑帖子关联
     */
    public GroupAlbumTopicDTO findGroupAlbumTopicById(Long id);

    /**
     * 查询小组专辑帖子关联列表
     * 
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 小组专辑帖子关联集合
     */
    public Pager<GroupAlbumTopicDTO> selectGroupAlbumTopicPager(GroupAlbumTopic groupAlbumTopic, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的小组专辑帖子关联列表
     *
     * @param groupAlbumTopic 小组专辑帖子关联
     * @return 小组专辑帖子关联集合
     */
    public List<GroupAlbumTopicDTO> exportGroupAlbumTopicList(GroupAlbumTopic groupAlbumTopic);
}
