package com.flycms.modules.elastic.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.elastic.domain.Synonym;
import org.springframework.stereotype.Repository;

/**
 * 同义词词库Mapper接口
 * 
 * @author admin
 * @date 2020-10-22
 */
@Repository
public interface SynonymMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增同义词词库
     *
     * @param synonym 同义词词库
     * @return 结果
     */
    public int insertSynonym(Synonym synonym);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除同义词词库
     *
     * @param id 同义词词库ID
     * @return 结果
     */
    public int deleteSynonymById(Long id);

    /**
     * 批量删除同义词词库
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSynonymByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改同义词词库
     *
     * @param synonym 同义词词库
     * @return 结果
     */
    public int updateSynonym(Synonym synonym);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验同义词是否唯一
     *
     * @param synonym 同义词词库ID
     * @return 结果
     */
    public int checkSynonymSynonymWordUnique(Synonym  synonym);


    /**
     * 查询同义词词库
     * 
     * @param id 同义词词库ID
     * @return 同义词词库
     */
    public Synonym findSynonymById(Long id);

    /**
     * 查询同义词词库数量
     *
     * @param pager 分页处理类
     * @return 同义词词库数量
     */
    public int querySynonymTotal(Pager pager);

    /**
     * 查询同义词词库列表
     * 
     * @param pager 分页处理类
     * @return 同义词词库集合
     */
    public List<Synonym> selectSynonymPager(Pager pager);

    /**
     * 查询所有同义词词库列表
     *
     * @return
     */
    public List<Synonym> selectSynonymList();
}
