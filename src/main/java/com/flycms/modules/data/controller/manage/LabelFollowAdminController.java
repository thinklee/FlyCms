package com.flycms.modules.data.controller.manage;


import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.data.domain.LabelFollow;
import com.flycms.modules.data.domain.dto.LabelFollowDTO;
import com.flycms.modules.data.service.ILabelFollowService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 话题关注Controller
 * 
 * @author admin
 * @date 2021-02-01
 */
@RestController
@RequestMapping("/system/data/labelFollow")
public class LabelFollowAdminController extends BaseController
{
    @Autowired
    private ILabelFollowService labelFollowService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增话题关注
     */
    @PreAuthorize("@ss.hasPermi('data:labelFollow:add')")
    @Log(title = "话题关注", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LabelFollow labelFollow)
    {
        return toAjax(labelFollowService.insertLabelFollow(labelFollow));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除话题关注
     */
    @PreAuthorize("@ss.hasPermi('data:labelFollow:remove')")
    @Log(title = "话题关注", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(labelFollowService.deleteLabelFollowByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题关注
     */
    @PreAuthorize("@ss.hasPermi('data:labelFollow:edit')")
    @Log(title = "话题关注", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LabelFollow labelFollow)
    {
        return toAjax(labelFollowService.updateLabelFollow(labelFollow));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询话题关注列表
     */
    @PreAuthorize("@ss.hasPermi('data:labelFollow:list')")
    @GetMapping("/list")
    public TableDataInfo list(LabelFollow labelFollow,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<LabelFollowDTO> pager = labelFollowService.selectLabelFollowPager(labelFollow, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出话题关注列表
     */
    @PreAuthorize("@ss.hasPermi('data:labelFollow:export')")
    @Log(title = "话题关注", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(LabelFollow labelFollow)
    {
        List<LabelFollowDTO> labelFollowList = labelFollowService.exportLabelFollowList(labelFollow);
        ExcelUtil<LabelFollowDTO> util = new ExcelUtil<LabelFollowDTO>(LabelFollowDTO.class);
        return util.exportExcel(labelFollowList, "labelFollow");
    }

    /**
     * 获取话题关注详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:labelFollow:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(labelFollowService.findLabelFollowById(id));
    }

}
