package com.flycms.modules.score.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.score.domain.ScoreDetail;
import com.flycms.modules.score.domain.dto.ScoreDetailDTO;

import java.util.List;

public interface IScoreDetailService {
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 保存用户积分记录
     *
     * @param scoreDetail
     *        积分记录实体
     * @param calculate
     *        运算：plus是加+,reduce是减-
     * @return
     */
    public int saveScoreDetail(ScoreDetail scoreDetail, String calculate);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除积分日志
     *
     * @param ids 需要删除的积分日志ID
     * @return 结果
     */
    public int deleteScoreDetailByIds(Long[] ids);

    /**
     * 删除积分日志信息
     *
     * @param id 积分日志ID
     * @return 结果
     */
    public int deleteScoreDetailById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改奖励记录
     *
     * @param id
     *
     */
    public void scoreDetailByCancel(Long id);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询积分日志
     *
     * @param id 积分日志ID
     * @return 积分日志
     */
    public ScoreDetailDTO findScoreDetailById(Long id);

    /**
     * 是否能奖励，true表示可以奖励
     * @param userId
     * @param scoreRuleId
     * @param type
     * @return
     */
    public boolean scoreDetailCanBonus(Long userId, Long scoreRuleId, String type);

    /**
     * 根据会员、获取奖励的外键、奖励规则ID获取奖励激励，不包括foreign_id=0
     * @param userId
     * @param scoreRuleId
     * @param forgignId
     * @return
     */
    public ScoreDetail findByForeignAndRule(Long userId, Long scoreRuleId, Long forgignId);

    /**
     * 查询积分日志列表
     *
     * @param scoreDetail 积分日志
     * @return 积分日志
     */
    public Pager<ScoreDetailDTO> selectScoreDetailPager(ScoreDetail scoreDetail, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的积分日志列表
     *
     * @param scoreDetail 积分日志
     * @return 积分日志集合
     */
    public List<ScoreDetailDTO> exportScoreDetailList(ScoreDetail scoreDetail);
}
