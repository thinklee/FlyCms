package com.flycms.modules.system.mapper;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.system.domain.FlyRole;
import org.springframework.stereotype.Repository;

/**
 * 角色表 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyRoleMapper
{
    /**
     * 根据条件查询角色数量
     *
     * @param pager 翻页类
     * @return 线索数量
     */
    public int queryRoleTotal(Pager pager);
    /**
     * 根据条件分页查询角色数据
     * 
     * @param pager 分页类
     * @return 角色数据集合信息
     */
    public List<FlyRole> selectRolePager(Pager pager);

    /**
     * 根据用户ID查询角色
     * 
     * @param adminId 用户ID
     * @return 角色列表
     */
    public List<FlyRole> selectRolePermissionByAdminId(Long adminId);

    /**
     * 查询所有角色
     * 
     * @return 角色列表
     */
    public List<FlyRole> selectRoleAll();

    /**
     * 根据用户ID获取角色选择框列表
     * 
     * @param adminId 用户ID
     * @return 选中角色ID列表
     */
    public List<Integer> selectRoleListByAdminId(Long adminId);

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param adminId 用户ID
     * @return 选中角色ID列表
     */
    public List<FlyRole> selectRoleByAdminId(Long adminId);

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public FlyRole selectRoleById(Long roleId);

    /**
     * 根据用户ID查询角色
     * 
     * @param adminName 用户名
     * @return 角色列表
     */
    public List<FlyRole> selectRolesByAdminName(String adminName);

    /**
     * 校验角色名称是否唯一
     * 
     * @param roleName 角色名称
     * @return 角色信息
     */
    public FlyRole checkRoleNameUnique(String roleName);

    /**
     * 校验角色权限是否唯一
     * 
     * @param roleKey 角色权限
     * @return 角色信息
     */
    public FlyRole checkRoleKeyUnique(String roleKey);

    /**
     * 修改角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int updateRole(FlyRole role);

    /**
     * 新增角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int insertRole(FlyRole role);

    /**
     * 通过角色ID删除角色
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleById(Long roleId);

    /**
     * 批量删除角色信息
     * 
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    public int deleteRoleByIds(Long[] roleIds);
}
