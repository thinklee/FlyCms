package com.flycms.modules.system.service.impl;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.modules.system.domain.Region;
import com.flycms.modules.system.domain.dto.RegionDTO;
import com.flycms.modules.system.mapper.RegionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.system.service.IRegionService;

import java.util.List;

/**
 * 行政区域Service业务层处理
 * 
 * @author admin
 * @date 2020-05-31
 */
@Service
public class RegionServiceImpl implements IRegionService 
{
    @Autowired
    private RegionMapper regionMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增行政区域
     *
     * @param region 行政区域
     * @return 结果
     */
    @Override
    public int insertRegion(Region region)
    {
        region.setId(SnowFlakeUtils.nextId());
        return regionMapper.insertRegion(region);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除行政区域
     *
     * @param ids 需要删除的行政区域ID
     * @return 结果
     */
    @Override
    public int deleteRegionByIds(Long[] ids)
    {
        return regionMapper.deleteRegionByIds(ids);
    }

    /**
     * 删除行政区域信息
     *
     * @param id 行政区域ID
     * @return 结果
     */
    @Override
    public int deleteRegionById(Long id)
    {
        return regionMapper.deleteRegionById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改行政区域
     *
     * @param region 行政区域
     * @return 结果
     */
    @Override
    public int updateRegion(Region region)
    {
        return regionMapper.updateRegion(region);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验行政区域名称是否唯一
     *
     * @param region 行政区域
     * @return 结果
     */
     @Override
     public String checkRegionRegionNameUnique(Region region)
     {
         int count = regionMapper.checkRegionRegionNameUnique(region);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }


    /**
     * 查询行政区域
     * 
     * @param id 行政区域ID
     * @return 行政区域
     */
    @Override
    public Region selectRegionById(Long id)
    {
        return regionMapper.selectRegionById(id);
    }


        /**
         * 查询行政区域所有列表
         *
         * @param region 行政区域
         * @return 行政区域
         */
        @Override
        public List<RegionDTO> selectRegionList(Region region)
        {
            List<Region> regionlist=regionMapper.selectRegionList(region);
            return BeanConvertor.copyList(regionlist, RegionDTO.class);
        }

}
