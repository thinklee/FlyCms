package com.flycms.modules.user.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

/**
 * 用户用小程序注册信息数据传输对象 fly_user_app
 * 
 * @author admin
 * @date 2020-05-30
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserAppDto
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** openid */
    @Excel(name = "openid")
    private String openId;
    /** unionid */
    @Excel(name = "unionid")
    private String unionId;
    /** 平台 */
    @Excel(name = "平台")
    private String platform;
    /** 头像 */
    @Excel(name = "头像")
    private String avatarUrl;
    /** 昵称 */
    @Excel(name = "昵称")
    private String nickName;
    /** 性别 */
    @Excel(name = "性别")
    private Integer gender;
    /** 所在国家 */
    @Excel(name = "所在国家")
    private String country;
    /** 所在省份 */
    @Excel(name = "所在省份")
    private String province;
    /** 所在城市 */
    @Excel(name = "所在城市")
    private String city;
    /** 使用的语言 */
    @Excel(name = "使用的语言")
    private String language;
}
