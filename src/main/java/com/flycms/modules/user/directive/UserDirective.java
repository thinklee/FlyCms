package com.flycms.modules.user.directive;

import com.flycms.common.utils.SessionUtils;
import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.domain.UserFans;
import com.flycms.modules.user.domain.dto.UserInfoDTO;
import com.flycms.modules.user.domain.vo.UserInfoVO;
import com.flycms.modules.user.service.IUserFansService;
import com.flycms.modules.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class UserDirective extends BaseTag {

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserFansService userFansService;

    public UserDirective() {
        super(UserDirective.class.getName());
    }

    //用户查询列表
    public Object paginate(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        String nickname = getParam(params, "nickname");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        int status = getStatusParam(params,"status");
        int deleted = getDeletedTopic(params);
        User user = new User();
        user.setNickname(nickname);
        user.setStatus(status);
        user.setDeleted(deleted);
        return userService.selectUserPager(user, pageNum, pageSize, sort, order);
    }

    //登录状态
    public Object userLogin(Map params) {
        User user = SessionUtils.getUser();
        return user;
    }

    //用户信息
    public Object user(Map params) {
        Long userId = getLongParam(params, "id");
        UserInfoDTO user = null;
        if(userId != null){
            user = userService.findUserInfoById(userId);
        }
        return user;
    }

    /**
     * 标签详细信息
     *
     * @param params
     * @return
     */
    public Object checkFollow(Map params) {
        Long id = getLongParam(params, "id");
        Long userId = getLongParam(params, "userId");
        if(id == null && userId ==null){
            return false;
        }
        Boolean label = userFansService.checkUserFansUnique(id,userId);
        return label;
    }

    //用户关注列表
    public Object followList(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        Long followId = getLongParam(params, "followId");
        Long fansId = getLongParam(params, "fansId");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        int status = getStatusParam(params,"status");
        UserFans userFans = new UserFans();
        userFans.setFollowId(followId);
        userFans.setFansId(fansId);
        return userFansService.selectUserFansPager(userFans, pageNum, pageSize, sort, order);
    }
}
