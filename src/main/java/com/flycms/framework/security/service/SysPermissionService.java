package com.flycms.framework.security.service;

import com.flycms.modules.system.domain.FlyAdmin;
import com.flycms.modules.system.service.IFlyMenuService;
import com.flycms.modules.system.service.IFlyRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * 用户权限处理
 * 
 * @author kaifei sun
 */
@Component
public class SysPermissionService
{
    @Autowired
    private IFlyRoleService roleService;

    @Autowired
    private IFlyMenuService menuService;

    /**
     * 获取角色数据权限
     * 
     * @param user 用户信息
     * @return 角色权限信息
     */
    public Set<String> getRolePermission(FlyAdmin user)
    {
        Set<String> roles = new HashSet<String>();
        // 管理员拥有所有权限
        if (user.isAdmin())
        {
            roles.add("admin");
        }
        else
        {
            roles.addAll(roleService.selectRolePermissionByUserId(user.getAdminId()));
        }
        return roles;
    }

    /**
     * 获取菜单数据权限
     * 
     * @param user 用户信息
     * @return 菜单权限信息
     */
    public Set<String> getMenuPermission(FlyAdmin user)
    {
        Set<String> perms = new HashSet<String>();
        // 管理员拥有所有权限
        if (user.isAdmin())
        {
            perms.add("*:*:*");
        }
        else
        {
            perms.addAll(menuService.selectMenuPermsByAdminId(user.getAdminId()));
        }
        return perms;
    }
}
