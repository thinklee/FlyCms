package com.flycms.framework.aspectj.lang.enums;

/**
 * 操作状态
 * 
 * @author kaifei sun
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
