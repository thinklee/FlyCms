$(document).ready(function(){
    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $(document).on('click', '#setting-btn', function (){
        var nickname=$("#nickname").val();
        var gender=$("input[name='gender']:checked").val();
        var signature=$("#signature").val();
        var about=$("#about").val();
        $.ajax({
            url:'/user/my/setting',
            type:'post',
            data:{'nickname':nickname,'gender':gender,"signature":signature,"about":about},
            dataType:'json',
            success:function(data){
                if(data.code==200){
                    layer.msg("修改成功", { shift: -1 }, function () {
                        window.location.reload();
                    });
                }else{
                    alert(data.msg);
                }
            },
            error:function(){
                console.log('请求出错！');
            }
        })
    });

    $(window).load(function() {
        var options =
            {
                thumbBox: '.thumbBox',
                spinner: '.spinner',
                dragMode : "move",
                background : true,// 容器是否显示网格背景
                movable : true,//是否能移动图片
                cropBoxMovable :false,//是否允许拖动裁剪框
                cropBoxResizable :false,//是否允许拖动 改变裁剪框大小
                imgSrc: '/images/avatar.jpg'
            }
        var cropper = $('.imageBox').cropbox(options);
        $('#upload-file').on('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                options.imgSrc = e.target.result;
                cropper = $('.imageBox').cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        $('#btnCrop').on('click', function(){
            var img = cropper.getDataURL();
            $('.cropped').html('');
            $('.cropped').append('<img src="'+img+'" align="absmiddle" style="width:64px;margin-top:4px;border-radius:64px;box-shadow:0px 0px 12px #7E7E7E;" ><p>64px*64px</p>');
            $('.cropped').append('<img src="'+img+'" align="absmiddle" style="width:128px;margin-top:4px;border-radius:128px;box-shadow:0px 0px 12px #7E7E7E;"><p>128px*128px</p>');
            $('.cropped').append('<img src="'+img+'" align="absmiddle" style="width:180px;margin-top:4px;border-radius:180px;box-shadow:0px 0px 12px #7E7E7E;"><p>180px*180px</p>');
            if (!img) {
                alert("请先选择图片");
            } else {
                $.ajax({
                    url: '/user/update/avatar',
                    async: false,
                    cache: false,
                    method: 'post',
                    dataType: 'json',
                    data: {
                        avatar: img
                    },
                    success: function(data) {
                        if (data.code == 200) {
                            layer.msg("头像修改成功！", {icon: 1});
                            var new_src = data.url+'?v='+Math.random();
                            $("#user_avatar_image,.avatar-32").attr("src",new_src);
                            $('#avatar_modal').modal('hide');
                        } else {
                            layer.msg(data.msg, {icon: 5});
                        }
                    }
                });
            }
        })
        $('#btnZoomIn').on('click', function(){
            cropper.zoomIn();
        })
        $('#btnZoomOut').on('click', function(){
            cropper.zoomOut();
        })
    });

});