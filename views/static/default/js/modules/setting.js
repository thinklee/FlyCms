$(document).ready(function(){
    $("#security_btn").on("click", function(){
        var old_password=$("#input-password-old").val();
        var password=$("#input-password-new").val();
        var re_password=$("#input-password-re-new").val();
        $.ajax({
            url: '/user/setting/security',
            data: {'old_password': old_password,'password': password,'re_password': re_password},
            dataType: "json",
            type :  "POST",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success: function(data){
                if(data.code==200){
                    layer.msg(data.msg, {shade: 0.2,icon: 1});
                    window.location.reload();
                }else{
                    layer.msg(data.msg, {shade: 0.2,icon: 2});
                    return false;
                }
            }
        });
    });


});